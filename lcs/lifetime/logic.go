package lifetime

import (
	"gitee.com/micro-plat/lcs/lcs/logger"
)

//Create 新增生命周期 数据(由于要链式调用,不能返回错误)
func Create(info *LifeTimeInfo) {
	defer func() {
		if v := recover(); v != nil {
			logger.LcsLogger.Infof("保存生命周期出错:%+v", v)
		}
	}()
	if err := info.Valid(); err != nil {
		panic(err)
	}

	dbCreate(info)
}

//Delete 删除生命周期
func Delete(expireTime string) error {
	return delete(expireTime)
}
