package logger

import (
	"sync"

	"github.com/micro-plat/lib4go/logger"
)

var LcsLogger *logger.Logger
var onceLock sync.Once

func Init() {
	onceLock.Do(func() {
		LcsLogger = logger.New("lcs")
	})
}
