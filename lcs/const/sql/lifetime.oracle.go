// +build oracle

package sql

const SaveLifeTimeInfo = `
insert into lcs_life_time
	(id,order_no, batch_no, extral_param,content, ip)
values
	(seq_life_time_id.nextval, @order_no,@batch_no, @extral_param,@content, @ip)
`

const DeleteLifeTimeInfo = `
delete from lcs_life_time
where
create_time < to_date(@create_time,'yyyy-mm-dd hh24:mi:ss')
`
