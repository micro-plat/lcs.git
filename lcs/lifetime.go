package lcs

import (
	"fmt"

	"gitee.com/micro-plat/lcs/lcs/lifetime"
	"github.com/asaskevich/govalidator"
)

//DeleteLifeTime 删除数据
//expireTime 过期时间 格式"2006-01-02 15:04:05"
func DeleteLifeTime(expireTime string) error {
	if !govalidator.IsTime(expireTime, "2006-01-02 15:04:05") {
		return fmt.Errorf("参数过期时间格式不正确")
	}
	return lifetime.Delete(expireTime)
}
